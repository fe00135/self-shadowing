# Copyright (C) 2024 University of Surrey.
# The code repository is published under the CC-BY-NC 4.0 license (https://creativecommons.org/licenses/by-nc/4.0/deed.en).

# This repository is the code of the paper "Learning Self-Shadowing for Clothed Human Bodies", by Farshad Einabadi, Jean-Yves Guillemaut and Adrian Hilton, in The 35th Eurographics Symposium on Rendering, London, England, 2024, proceedings published by Eurographics - The European Association for Computer Graphics.

# Parts of the code was adapted from Yoshihiro Kanamori, Yuki Endo: "Relighting Humans: Occlusion-Aware Inverse Rendering
    # for Full-Body Human Images," ACM Transactions on Graphics (Proc. of SIGGRAPH Asia 2018)
    # https://kanamori.cs.tsukuba.ac.jp/projects/relighting_human


import torch
import torch.nn.functional as F
import torch.nn as nn
from torchsummary import summary


class ResidualBlock(nn.Module):
    def __init__(self, n_in, n_out, stride=1, kernel_size=7, padding=1):
        super().__init__()

        self.c0 = nn.Conv2d(n_in, n_out,  kernel_size=kernel_size, stride=stride, padding='same')
        nn.init.normal_(self.c0.weight, 0.0, 0.02)
        self.c1 = nn.Conv2d(n_out, n_out, kernel_size=kernel_size, stride=stride, padding='same')
        nn.init.normal_(self.c1.weight, 0.0, 0.02)
        self.bnc0 = nn.InstanceNorm2d(n_out)
        self.bnc1 = nn.InstanceNorm2d(n_out)

    def forward(self, x):
        h = F.leaky_relu(self.bnc0(self.c0(x))) + x
        return F.leaky_relu(self.bnc1(self.c1(h))) + h


class EncoderBlock(nn.Module):
    def __init__(self, n_in, n_out, kernel_size=3):
        super().__init__()

        self.c0 = nn.Conv2d(n_in, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c0.weight, 0.0, 0.02)
        self.c1 = nn.Conv2d(n_out, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c1.weight, 0.0, 0.02)
        self.c2 = nn.Conv2d(n_out, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c2.weight, 0.0, 0.02)
        self.c3 = nn.Conv2d(n_out, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c3.weight, 0.0, 0.02)
        self.c4 = nn.Conv2d(n_out, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c4.weight, 0.0, 0.02)
        self.c5 = nn.Conv2d(n_out, n_out, kernel_size, stride=2, padding=1)
        nn.init.normal_(self.c5.weight, 0.0, 0.02)
        self.bnc0 = nn.InstanceNorm2d(n_out)
        self.bnc1 = nn.InstanceNorm2d(n_out)
        self.bnc2 = nn.InstanceNorm2d(n_out)
        self.bnc3 = nn.InstanceNorm2d(n_out)
        self.bnc4 = nn.InstanceNorm2d(n_out)
        self.bnc5 = nn.InstanceNorm2d(n_out)

    def forward(self, x):
        h = F.leaky_relu(self.bnc0(self.c0(x)))
        h = F.leaky_relu(self.bnc1(self.c1(h)))
        h = F.leaky_relu(self.bnc2(self.c2(h)))
        h = F.leaky_relu(self.bnc3(self.c3(h)))
        h = F.leaky_relu(self.bnc4(self.c4(h)))
        return F.leaky_relu(self.bnc5(self.c5(h)))


class DecoderBlock(nn.Module):
    def __init__(self, n_in, n_out, kernel_size=3):
        super().__init__()

        self.c0 = nn.Conv2d(n_in, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c0.weight, 0.0, 0.02)
        self.c1 = nn.Conv2d(n_out, n_out, kernel_size, padding='same')
        nn.init.normal_(self.c1.weight, 0.0, 0.02)
        self.bnc0 = nn.InstanceNorm2d(n_out)
        self.bnc1 = nn.InstanceNorm2d(n_out)
        self.us = nn.UpsamplingBilinear2d(scale_factor=2)

    def forward(self, x):
        h = F.leaky_relu(self.bnc0(self.c0(x)))
        h = F.leaky_relu(self.bnc1(self.c1(h)))
        return self.us(h)


class Decoder(nn.Module):
    def __init__(self, input_channels):
        super().__init__()

        self.dcb0 = DecoderBlock(input_channels, 256)
        self.dcb1 = DecoderBlock(256+256, 128)
        self.dcb2 = DecoderBlock(128+128, 64)
        self.dcb3 = DecoderBlock(64+64, 32)
        self.dcb4 = DecoderBlock(32+32, 32)
        self.dcf = nn.Conv2d(32, 1, kernel_size=3, padding='same')

    def forward(self, x, he3, he2, he1, he0):

        ha = self.dcb0(x)

        ha = torch.cat((ha, he3), 1)  # spatial 32
        ha = self.dcb1(ha)

        ha = torch.cat((ha, he2), 1)  # spatial 64
        ha = self.dcb2(ha)

        ha = torch.cat((ha, he1), 1)  # spatial 128
        ha = self.dcb3(ha)

        ha = torch.cat((ha, he0), 1)  # spatial 256
        ha = self.dcb4(ha)

        ha = self.dcf(ha)

        return ha


class MHASelfAttentionLayer(nn.Module):
    " Not shared, not used in this distribution"

class RelightNet(nn.Module):

    def __init__(self, mha_number_heads, number_input_channels, final_tanh):
        super(RelightNet, self).__init__()

        self._final_tanh = final_tanh

        self._mha_number_heads = mha_number_heads

        if mha_number_heads > 0:
            self.enmha0 = MHASelfAttentionLayer(256, mha_number_heads)
            self.enmha1 = MHASelfAttentionLayer(256, mha_number_heads)
            self.enmha2 = MHASelfAttentionLayer(256, mha_number_heads)
            self.enmha3 = MHASelfAttentionLayer(256, mha_number_heads)

        self.enb0 = EncoderBlock(number_input_channels, 32)
        self.enb1 = EncoderBlock(32, 64)
        self.enb2 = EncoderBlock(64, 128)
        self.enb3 = EncoderBlock(128, 256)
        self.enb4 = EncoderBlock(256, 512)

        self.residual_block = ResidualBlock(512, 512)

        self.decoder_pos = Decoder(512)

    def forward(self, x, is_train=True):
        # encoder
        he0 = self.enb0(x)    # spatial 256
        he1 = self.enb1(he0)  # spatial 128
        he2 = self.enb2(he1)  # spatial 64
        he3 = self.enb3(he2)  # spatial 32
        he4 = self.enb4(he3)  # spatial 16
        hr = F.dropout(he4, 0.1, training=is_train)

        # latent
        if self._mha_number_heads > 0:
            ps = 16  # patch_size
            patches = hr.view(hr.shape[0], hr.shape[1], ps*ps)
            patches = self.enmha0(patches)
            patches = self.enmha1(patches)
            patches = self.enmha2(patches)
            patches = self.enmha3(patches)
            hr = patches.view(patches.shape[0], patches.shape[1], ps, ps)
        else:
            hr = self.residual_block(hr)

        ha_pos = self.decoder_pos(hr, he3, he2, he1, he0)

        if self._final_tanh:
            ha_pos = torch.tanh(ha_pos)

        return ha_pos


if __name__ == "__main__":
    number_in_ch = 9
    model = RelightNet(2, number_in_ch, final_tanh=True)
    model = model.to("cuda:0")
    print(model)
    res = 512
    summary(model, (number_in_ch, res, res))
    test_image = torch.rand((2, number_in_ch, res, res), device="cuda:0")
    model.forward(test_image, is_train=False)
