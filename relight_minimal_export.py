# Copyright (C) 2024 University of Surrey.
# The code repository is published under the CC-BY-NC 4.0 license (https://creativecommons.org/licenses/by-nc/4.0/deed.en).

# This repository is the code of the paper "Learning Self-Shadowing for Clothed Human Bodies", by Farshad Einabadi, Jean-Yves Guillemaut and Adrian Hilton, in The 35th Eurographics Symposium on Rendering, London, England, 2024, proceedings published by Eurographics - The European Association for Computer Graphics.


import logging
import argparse
import os
import time
from pathlib import Path

import yaml

import torch
from torchvision import transforms
from torchvision.utils import save_image

import numpy as np
import cv2

from PIL import Image

import functools

from relight_net import RelightNet
from pifuhd_front_normals import GlobalGenerator


logger = logging.getLogger('relight_minimal_export')


class RelightMinimal():
    """" Minimal export for the inference part of the diffuse relighting algorithm for people """

    def __init__(self, device, pifuhd_netf_path, self_shadow_model_path, relight_model_path):

        self._normals_net = self._load_surface_normal_estimator(pifuhd_netf_path)
        self._self_shadow_net = self._load_relighting_model(self_shadow_model_path, number_input_channels=9)
        self._relight_net = self._load_relighting_model(relight_model_path, number_input_channels=10)

        for model in [self._normals_net, self._self_shadow_net, self._relight_net]:
            model = model.to(device=device)
            model.eval()

    def _load_surface_normal_estimator(self, pretrained_model_path):
        norm_layer = functools.partial(torch.nn.InstanceNorm2d, affine=False)
        model = GlobalGenerator(3, 3, 64, 4, 9, norm_layer, last_op=torch.nn.Tanh())
        model.load_state_dict(torch.load(pretrained_model_path))
        return model

    def _load_relighting_model(self, pretrained_model_path, number_input_channels):
        model = RelightNet(mha_number_heads=0, number_input_channels=number_input_channels, final_tanh=True)
        model_state = torch.load(pretrained_model_path)['mor_state']['model_state']
        model.load_state_dict(model_state)
        return model

    def run(self, input_rgb, input_mask, light_dirs_phi_theta):
        """"
            input_rgb [batch_size, 3, 512, 512]
            input_mask [batch_size, 1, 512, 512]
            light_dirs_phi_theta [batch_size, number_of_light_dirs, 2]
        """

        normals_estimated = self._normals_net(self._normalise_image_input(input_rgb)).detach()
        normals_estimated = torch.mul(input_mask, normals_estimated)

        light_dirs_unit_vec, shadings_initial = self._olats(light_dirs_phi_theta, normals_estimated)

        visibilities_estimated = []
        shadings_estimated = []

        batch_size = input_rgb.shape[0]
        number_light_dirs = light_dirs_phi_theta.shape[1]
        for i in range(number_light_dirs):
            image_size = 512
            encoder_input_light = light_dirs_unit_vec[:, i, :][:, :, None, None].repeat(1, 1, image_size, image_size)
            encoder_input_rgb = self._normalise_image_input(input_rgb)

            vis_encoder_input = [encoder_input_rgb, normals_estimated, encoder_input_light]
            vis_encoder_input = torch.cat(vis_encoder_input, dim=1)
            visibility_estimated = self._self_shadow_net(vis_encoder_input, is_train=False).detach()
            visibility_estimated = self._denormalise_image_output(visibility_estimated)
            visibility_estimated = torch.mul(input_mask, visibility_estimated)
            visibilities_estimated.append(visibility_estimated)

            shading_initial = torch.mul(shadings_initial[:, i:i+1, :, :], visibility_estimated)
            shading_initial = self._normalise_image_input(shading_initial)

            encoder_input = [normals_estimated, encoder_input_rgb, visibility_estimated, encoder_input_light]
            encoder_input = torch.cat(encoder_input, dim=1)
            shading_estimated = self._relight_net.forward(encoder_input, is_train=False).detach() + shading_initial
            shading_estimated = torch.clamp(shading_estimated, -1, 1)
            shading_estimated = self._denormalise_image_output(shading_estimated)
            shading_estimated = shading_estimated.view(batch_size, -1)
            max_shading_values = torch.max(shading_estimated, 1, keepdim=True)[0]
            shading_estimated = shading_estimated / max_shading_values
            shading_estimated = shading_estimated.view(batch_size, 1, image_size, image_size)
            shadings_estimated.append(shading_estimated)

        visibilities_estimated = torch.cat([c for c in visibilities_estimated], dim=1)
        shadings_estimated = torch.cat([c for c in shadings_estimated], dim=1)
        shadings_estimated = self._colour_code_input_output(shadings_estimated, input_mask)

        return shadings_estimated, visibilities_estimated, normals_estimated

    def _olats(self, olat_directions, normals):
        phis = olat_directions[:, :, 0]
        thetas = olat_directions[:, :, 1]
        olat_directions_vec = torch.stack((-torch.cos(phis)*torch.sin(thetas), torch.cos(thetas),
                                           -torch.sin(phis)*torch.sin(thetas)), dim=2)
        number_olats = olat_directions.shape[1]
        batch_size = normals.shape[0]
        input_size = normals.shape[2]
        olats = torch.matmul(olat_directions_vec, normals.flatten(2, 3)).reshape(
            batch_size, number_olats, input_size, input_size)
        olats = torch.clamp(olats, 0, 1)
        return olat_directions_vec, olats

    def _normalise_image_input(self, in_):
        return (in_ - 0.5) * 2

    def _denormalise_image_output(self, in_):
        return in_ / 2. + 0.5

    def _colour_code_input_output(self, in_, mask):
        return 1 - mask + in_


class InputSample():
    def __init__(self, rgb_image_orig, mask_image_orig):
        self.rgb_image_orig = np.array(rgb_image_orig)
        self.mask_image_orig = np.array(mask_image_orig)

    def crop_square(self, square_size):
        """ 
        Adapted from Yoshihiro Kanamori, Yuki Endo: "Relighting Humans: Occlusion-Aware Inverse Rendering
            for Full-Body Human Images," ACM Transactions on Graphics (Proc. of SIGGRAPH Asia 2018)
            https://kanamori.cs.tsukuba.ac.jp/projects/relighting_human
        """

        x_trimmed, y_trimmed, w_trimmed, h_trimmed = cv2.boundingRect(self.mask_image_orig)

        v_padding = int(0.2 * w_trimmed)
        w_square = 2 * v_padding + (w_trimmed if w_trimmed > h_trimmed else h_trimmed)

        x_square = int(0.5 * (w_square - w_trimmed))
        y_square = v_padding

        img_square = 255 * np.zeros((w_square, w_square, 3), dtype=np.uint8)
        mask_square = np.zeros((w_square, w_square), dtype=np.uint8)

        img_square[y_square:y_square+h_trimmed, x_square:x_square+w_trimmed, :] = self.rgb_image_orig[
            y_trimmed:y_trimmed+h_trimmed, x_trimmed:x_trimmed+w_trimmed, :]
        mask_square[y_square:y_square+h_trimmed, x_square:x_square + w_trimmed] = self.mask_image_orig[
            y_trimmed:y_trimmed+h_trimmed, x_trimmed:x_trimmed+w_trimmed]

        self.rgb_image_square = cv2.resize(img_square, (square_size, square_size))
        self.mask_image_square = cv2.resize(mask_square, (square_size, square_size))
        self._crop_size_position = np.array([w_square, y_trimmed-y_square, x_trimmed-x_square], dtype=np.int32)

    def to_tensor(self, device):
        def t(image): return (transforms.ToTensor()(image)).unsqueeze(0).to(device=device)
        self.rgb_image_orig = t(self.rgb_image_orig)
        self.mask_image_orig = t(self.mask_image_orig)
        self.mask_image_square = t(self.mask_image_square)
        self.rgb_image_square = t(self.rgb_image_square) * self.mask_image_square


def read_yaml(file_path):
    """
    :rtype: dict
    """
    try:
        with open(str(file_path)) as yaml_file:
            content = yaml.load(yaml_file, Loader=yaml.BaseLoader)
    except OSError as error:
        logger.error("Could not read the yaml file: %s", error)
        return None
    return content


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_path", help="Path to input directory.")
    parser.add_argument("output_path", help="Path to output directory. Will be created.")
    parser.add_argument("pifuhd_netf_path", help="Path to the pre-trained PIFuHD pre-trained normal estimator.")
    parser.add_argument("self_shadow_model_path", help="Path to the pre-trained pre-trained self-shadow model.")
    parser.add_argument("relight_model_path", help="Path to the pre-trained pre-trained relighting model.")
    parser.add_argument("-b", "--batch-size", help="Batch size.", type=int, default=2)
    parser.add_argument("--gpu", help="Use first gpu when available", default=False, action='store_true')
    args = parser.parse_args()

    desired_batch_size = args.batch_size
    device = "cuda:0" if args.gpu else "cpu"
    input_path = args.input_path
    output_path = args.output_path
    os.makedirs(output_path, exist_ok=True)

    entry_paths = list(sorted(Path(input_path).rglob('*_details.yaml')))

    input_samples = []
    input_light_dirs_phi_theta = []
    for entry_path in entry_paths:
        sample_path, filename = os.path.split(entry_path)
        filename, extension = os.path.splitext(filename)
        base_name = filename[:-len("_details")]
        try:
            details = read_yaml(entry_path)
            input_light_dirs_phi_theta.append(np.asarray(details["light_directions"], dtype=np.float32))
            rgb_path = os.path.join(sample_path, base_name + "_input.png")
            rgb_image_orig = Image.open(rgb_path)
            mask_path = os.path.join(sample_path, base_name + "_mask.png")
            mask_image_orig = Image.open(mask_path)
            input_sample = InputSample(rgb_image_orig, mask_image_orig)
            input_sample.crop_square(512)
            input_sample.to_tensor(device)
            input_samples.append(input_sample)
        except Exception as error:
            logger.error("Can not read the entry:", entry_path)

    input_light_dirs_phi_theta = np.array(input_light_dirs_phi_theta)
    input_light_dirs_phi_theta = torch.Tensor(input_light_dirs_phi_theta).to(device=device)
    number_light_dirs = input_light_dirs_phi_theta.shape[1]
    input_light_dirs_phi_theta[:, :, 0] += torch.pi

    input_images = []
    input_masks = []
    for sample in input_samples:
        input_images.append(sample.rgb_image_square)
        input_masks.append(sample.mask_image_square)
    input_image = torch.cat(input_images, dim=0)
    input_mask = torch.cat(input_masks, dim=0)

    start_time = time.time()
    model = RelightMinimal(device, args.pifuhd_netf_path, args.self_shadow_model_path, args.relight_model_path)
    end_time = time.time()

    number_input_samples = len(input_samples)
    number_rounds = int(torch.ceil(torch.tensor(number_input_samples, dtype=torch.float32) / desired_batch_size))
    print("# light directions %d, # input images %d, batch size %d, # rounds %d"
          % (number_light_dirs, number_input_samples, desired_batch_size, number_rounds))
    print("Loading model: %.3f sec" % (end_time-start_time))
    for r in range(number_rounds):
        r_slice = slice(desired_batch_size*r, min(desired_batch_size*(r+1), number_input_samples))
        batch_size = min(desired_batch_size*(r+1), number_input_samples) - desired_batch_size*r

        start_time = time.time()
        shadings_estimated, visibilities_estimated, normals_estimated = model.run(
            input_image[r_slice], input_mask[r_slice], input_light_dirs_phi_theta[r_slice])
        end_time = time.time()
        print("Inference round %d: %.3f sec" % (r, end_time-start_time))

        delimiter = "_"
        file_type = ".png"
        for b in range(batch_size):
            sample_index = desired_batch_size*r + b
            prefix = str(sample_index)
            for suffix, image in zip(["input", "mask", "normals"],
                                     [input_image[r_slice], input_mask[r_slice], normals_estimated]):
                save_image(image[b], os.path.join(output_path, prefix + delimiter + suffix + file_type))
            for suffix, buffer in zip(["shading", "vis"], [shadings_estimated, visibilities_estimated]):
                for i in range(number_light_dirs):
                    save_image(buffer[b, i:i+1, :, :], os.path.join(
                        output_path, prefix + delimiter + suffix + delimiter + str(i) + file_type))
