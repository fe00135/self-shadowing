FROM nvidia/cuda:11.0.3-cudnn8-devel-ubuntu18.04

RUN apt-get -y update

RUN DEBIAN_FRONTEND="noninteractive" apt-get install tzdata -y

RUN apt-get -y install wget libopencv-core-dev libopencv-dev

RUN apt-get autoremove -y && apt-get autoclean -y && rm -rf /var/lib/apt/lists/*

ENV WRKSPCE="/workspace"
RUN wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b -p $WRKSPCE/miniconda3 \
    && rm -f Miniconda3-latest-Linux-x86_64.sh
ENV PATH="$WRKSPCE/miniconda3/bin:${PATH}"
COPY environment.yaml .
RUN conda env create --prefix $WRKSPCE/venvs/torch --file environment.yaml \
    && conda clean -y --all

CMD /bin/bash
